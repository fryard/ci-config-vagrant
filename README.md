# mbed OS CI
This is contains all the Jenkins configuration for the mbed OS CI system. It mainly serves as a backup for all the jobs, nodes, and users of the system. It also serves as general documentation for how the system is distributed and maintained.

**There is a lot of material here, but please do not skip anything!** The system has lots of components. Reading this document should give you good idea how everything fits together, which is very important when debugging issues.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [System setup](#system-setup)
  - [Jenkins](#jenkins)
    - [GitHub credentials](#github-credentials)
    - [Syncing the Jenkins configuration](#syncing-the-jenkins-configuration)
  - [mbed GitHub bot](#mbed-github-bot)
    - [mbed-github-bot-bridge](#mbed-github-bot-bridge)
    - [mbed-github-bot](#mbed-github-bot)
  - [MongoDB build statistics service](#mongodb-build-statistics-service)
  - [Public Test results](#public-test-results)
- [System opperation](#system-opperation)
  - [Starting the system](#starting-the-system)
  - [Triggering CI runs from GitHub](#triggering-ci-runs-from-github)
  - [Jenkins Pipeline jobs](#jenkins-pipeline-jobs)
- [Building](#building)
  - [Adding a new build node](#adding-a-new-build-node)
    - [Machine setup](#machine-setup)
    - [Environment setup](#environment-setup)
    - [Jenkins node setup](#jenkins-node-setup)
      - [Connection](#connection)
    - [Configuration](#configuration)
        - [Labels](#labels)
      - [Environment variables](#environment-variables)
  - [Jenkins build jobs](#jenkins-build-jobs)
- [Testing](#testing)
  - [Current limitations](#current-limitations)
  - [Adding new mbed boards](#adding-new-mbed-boards)
    - [Creating a new test machine](#creating-a-new-test-machine)
    - [Adding to an existing test machines](#adding-to-an-existing-test-machines)
  - [Jenkins test jobs](#jenkins-test-jobs)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## System setup
The mbed OS CI is ran by a number services. Please see below for more details

### Jenkins
You will need to setup a Jenkins master server. Installation instructions can be found on the [Jenkins website](http://jenkins-ci.org).

The master server has always been ran on an Ubuntu server, so we would recommend using this environment for maximum compatibility.

#### GitHub credentials
The CI system will need to access to the ARMmbed GitHub repositories. The [mbed-bot](http://github.com/mbed-bot) is used to provide access. Please see Brian Daniels or Sam Grove for account credentials. It will need to be added to the Jenkins configuration.

**TODO** use a better way of sharing the account credentials

#### Syncing the Jenkins configuration
This repository contains most of the necessary Jenkins configuration to duplicate the CI setup. You will need to install the [SCM Sync Plugin](https://wiki.jenkins-ci.org/display/JENKINS/SCM+Sync+configuration+plugin) on your Jenkins master server to sync the configuration. Once installed, configure the plugin to look at this repository to being the sync.

### mbed GitHub bot
The mbed GitHub bot is used to trigger extensive builds and testing on PRs. It can also be used to customize what tests are ran and to implement custom commands (though none are currently in use).

The bot is actually made of two services: [mbed-github-bot-bridge](http://github.com/ARMmbed/mbed-github-bot-bridge) and [mbed-github-bot](http://github.com/ARMmbed/mbed-github-bot).

#### mbed-github-bot-bridge
*mbed-github-bot-bridge* must be setup first on a public server. It is used to forward GitHub webhook events to the main bot service, *mbed-github-bot*.

This is currently being ran on a VM in ARM mbed's Azure account. The VM is located at mbedci.cloudapp.net. Please see Brian Daniels or Sam Grove on obtaining access to this VM.

**TODO** use a better way of sharing the account credentials

#### mbed-github-bot
*mbed-github-bot* contains all of the configuration and scripts that determine the bot's behavior. Both repositories contain instructions on how to install, configure, and run them.

This is currently being ran on the same machine as the Jenkins master server. This is not a necessity, however it is necessary for the bot to be able to access the Jekins master's web API. This is used to start and track the progress of jobs. It must also be able to access the *mbed-github-bot-bridge* instance.

### MongoDB build statistics service
There is a MonogoDB instance running on the "Build Beast" machine (10.118.12.78). This is used to store build statistics which is then used for reporting. It should have a database named `mbedOSCI` and inside that a collection called `builds`. You shouldn't have to setup anything else as the services will do all the data insertion and querying.

### Public Test results
There a public test result server running in ARM mbed's Azure account. The address for this VM is mbedosci.cloudapp.net. This allows the CI system to SSH into the VM and upload build logs and test reports from the CI run. They can then be accessed from any network, not just the ARM network. For acccess credentials to the VM, please see Brian Daniels or Sam Grove.

**TODO** provide a better way of sharing credentials.

## System opperation

### Starting the system
Upon restarting the master server, all the services should restart by themselves. However if you ever need to manually start the services they should be started in the following order:

1. MonogoDB and Public test result server (this shouldn't require much, if any maintenance)
2. Jenkins master server
3. mbed-github-bot-bridge
4. mbed-github-bot

### Triggering CI runs from GitHub
Currently the following GitHub bot commands are available:

- `/morph echo` - Doesn't trigger any CI runs, but it does respond immediately with a GitHub comment. This is useful to ensure that `mbed-github-bot` and `mbed-github-bot-bridge` are working correctly.
- `/morph test` - The main PR test command. It builds mbed OS and its tests, it builds many example programs, and it runs the RTOS tests on all connected platforms
- `/morph test-nightly` - The equivalent of `/morph test`, but it runs all tests on hardware, not just the RTOS test. This is also run each night.
- `/morph export-build` - This will export and then build of some of the example programs.

### Jenkins Pipeline jobs
These are triggered by the GitHub bot. These are made up of other jobs that do the actual building and testing. It helps faciliatate parallelization. They also have nice visualization of the status of running and queued jobs

## Building
One of mbed's testing policies is "everything should always build". Since building is relatively "cheap" (both in time and resources) when compared to hardware tests, we try to build as much as possible. This section describes a few of the builds we do and how to add new build hardware to decrease build times.

### Adding a new build node
Adding a new build node allows use to parallelize jobs further, decreasing build times. A build node is just a normal Jenkins node with a few labels added. There are also a few assumptions made about the machine's environment.

#### Machine setup
The build machines should be visible to the master server on the network. I recommend enabling Remote Desktop on Windows machines and SSH on Linux machines for remote management.

#### Environment setup
Each build node is assumed to have certain tools and settings configured in its default environment.

The following software packages must be installed and available in the system PATH (via the specified command)

- [Python 2.7.x and pip](http://python.org) - via the command `python` and `pip` respectively
- [Java 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
    - This is necessary for Jenkins
    - For Ubuntu, I recommend this guide for install Oracle Java 8 via the package manager: [Install Oracle Java 8 In Ubuntu Or Linux Mint Via PPA Repository](http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html)
- [Git](http://git-scm.com) - via the command `git`
- sh - via the command `sh`
    - For Linux machines, this should be ready to go
    - For Windows machines, this comes bundled with Git. Just add the path `<Git install path>\bin` to your system PATH (ex. `C:\Program Files\Git\bin`). If `<Git install path>\cmd>` already exists in your system PATH, you may replace it with this new path
- [Mercurial](https://www.mercurial-scm.org) - via the command `hg`
- [virtualenv](https://pypi.python.org/pypi/virtualenv) - via the command `virtualenv`
    - Install via `pip install virtualenv`
- [mongo v2.4.14](https://www.mongodb.com/download-center#community) - via the command `mongo`
    - Used to upload build results directly to a MongoDB server
    - [Linux Downloads](https://www.mongodb.org/dl/linux/)
        - You can use the binaries linked above or you can use the package manager using the steps described here: [Install MongoDB on Ubuntu v2.4](https://docs.mongodb.com/v2.4/tutorial/install-mongodb-on-ubuntu/)
    - [Windows Downloads](https://www.mongodb.org/dl/win32)
        - Ex. Windows 64 bit 2.4.14 - [win32/mongodb-win32-x86_64-2.4.14.zip](win32/mongodb-win32-x86_64-2.4.14.zip)
- All compilers and IDEs
    - [GCC_ARM v4.9.3](https://launchpad.net/gcc-arm-embedded/4.9/4.9-2015-q3-update)
        - For Ubuntu:
            ```
            sudo add-apt-repository -y ppa:terry.guo/gcc-arm-embedded
            sudo apt-get -y update
            sudo apt-get -y install gcc-arm-none-eabi=4.9.3.2015q3*
            ```
            - Performance seems to be significantly better on Linux than on Windows, so take this into consideration when setting up your build nodes
        - For Windows, use the setup application linked above and ensure the binaries (the folder that contains `arm-none-eabi-gcc`) are in your system PATH
    - [ARM 5.06 update 4](https://developer.arm.com/products/software-development-tools/compilers/arm-compiler-5/downloads)
        - **Note:** A CI machine should not use the compiler bundled with uVision, as this does not support Cortex-A devices
        - The folder containing `armcc` should be in your system PATH
        - Performance seems to be significantly better on Linux than on Windows, so take this into consideration when setting up your build nodes
        - A Node-locked license is significantly faster than a network license when compiling. These can be obtained from the license support team by emailing license.support@arm.com.
        - [This doc](https://armh.sharepoint.com/sites/Iot-developer-enablement-o365/_layouts/15/guestaccess.aspx?guestaccesstoken=VULzr+rPUWncWyxmVQCBgXsyE1moc2cBkGMKTx2Mcxs=&docid=2_0d5ccd7e01cea4bf78d8f9aab6b396b7f&rev=1) tracks the currently used node-locked licenses
    - [IAR v7.8](https://www.iar.com/iar-embedded-workbench/#!?architecture=ARM)
        - **Note:** This seems to be the slowest compiler by almost an order of magnitude, so it may be good to equip most of the CI machines with this if possible to reduce bottlenecking the build
        - Only supported on Windows. There is a network license server available for use. Please see [this document](https://armh.sharepoint.com/sites/Iot-developer-enablement-o365/_layouts/15/guestaccess.aspx?guestaccesstoken=DsEj48md8I8qFS5j69DZ/KrGH3W9IGUHhgRQ0VAr3l0=&docid=2_0e1aff338befe4ebcb6d67f96f91a2111&rev=1) for up-to-date information on the license server
        - You should place the directory that contains `iccarm.exe` in your system PATH for the mbed build system. This path is `<iar installation path>\arm\bin` (ex. `C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.5\arm\bin`)
        - You should place the directory that contains `IARBuild.exe` in your system PATH for exporter tests. This path is `<iar installation path>\common\bin\IarBuild` (ex. `C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.5\common\bin\IarBuild.exe`)
    - [Keil uVision 5](https://www.keil.com/demo/eval/arm.htm)
        - Seems to work fine with the network license (easier to manage), however you can use a dedicated license if you find it helps
        - You should also ensure uVision's build system executable (`UV4.exe`) is in your system path to enable exporter tests. This path is `<uvision installation path>\UV4` (ex. `C:\Keil_v5\UV4`)
    
    - [Make](https://www.gnu.org/software/make/k)
        - For Linux, install via the package manager (Ubuntu: `sudo apt-get install make`)
            - You'll also need `srec_cat` for some targets that merge hex files. This can be installed via the package manager (`sudo apt-get install srecord`)
        - For Windows, install from [here](http://gnuwin32.sourceforge.net/packages/make.htm)
            - You'll also need `srec_cat` for some targets that merge hex files. This can be installed via a Windows build of srecord: [SourceForge](https://sourceforge.net/projects/srecord/files/srecord-win32/1.64/)
                - Be sure to place the directory containing `srec_cat.exe` in your system PATH

#### Jenkins node setup

##### Connection
Connect the machine to Jenkins by creating a new node in the Jenkins web UI. For Linux build nodes (used for GCC_ARM and ARM toolchains, as well as some Make exporters), use an SSH connection. For Windows build nodes (used mainly for IAR and desktop IDEs), use a JNLP connection (be sure to install it as a service as well).

#### Configuration
In the node's configuration page in Jenkins, be sure to set `# of executors` to `2`. Also be sure to give it a unique `Remote root directory`

###### Labels
Once connected to Jenkins, you need to add some labels to the node so Jenkins knows its capabilities. For build nodes, you need to add the `build_node` label, as well `toolchain_*` and `ide_*` labels that match what is installed on the machine. For instance, if the machine has the GCC_ARM and IAR compilers installed, as well as Make and uVision installed, you would add the following labels:

```
build_node toolchain_GCC_ARM toolchain_IAR ide_make_gcc_arm ide_make_iar ide_uvision
```

**NOTE:** Toolchain names are uppercase, IDE names are lower case.

##### Environment variables
Some of the jobs need to know if the node is a Windows machines, so you need to add an environment variable here with the name `OS_NAME`. The value should be `windows` for a Windows machine and `linux` for a Linux machine.

You can add extra environment variables here if you need them as well, though there may be some things that are more appropriate to place on the system itself rather than just the Jenkins process.

### Jenkins build jobs
This section outlines a few of the build jobs that are currently in use

- `bm_wrap` - This job finds all online build nodes, clones mbed-os, and creates a virtualenv for them with all the necessary Python dependencies
- `examples_wrap` - This job uses the environment that `bm_wrap` creates and clones all examples detailed in the JSON file located in the mbed-os repository (currently this is `mbed-os/tools/test/examples/examples.json`). It also replaces all instances of the mbed-os repository with a symbolically linked version of mbed-os to that which was cloned in the `bm_wrap` job.
- `build_matrix` - This job builds mbed OS 5 and its tests for all targets and toolchains. If a target is listed as supported by `mbed compile --supported`, it will be included in the build.
- `examples_build_matrix` - This job builds a subset of the mbed OS examples for most targets and toolchains. This uses the tools located in `mbed-os/tools/test/examples` and is driven by `examples.json`
- `export_build_matrix` - This job exports and builds a subset of the mbed OS examples for most targets and IDEs. This uses the tools located in `mbed-os/tools/test/examples` and is driven by `examples.json`. If the target and IDE show up as supported by `mbed export --supported`, it well be included in the build
- `build_matrix_wrapper_cleanup` - This job cleans up a given environment created by `bm_wrap`.


## Testing
This section covers hardware testing for mbed OS.

### Current limitations
Due to a number of reasons (include interface firmware bugs, USB hub quirks, OS and kernal USB drivers, etc), parallel testing mbeds is unreliable. Work is still on going to fix this, but for the time being we can only test one platform per test machine at a time reliably. For example, if you have 3 test machines and 9 boards, you can at most have three tests running across the whole system at once if you plug in 3 boards into each machine/

### Adding new mbed boards
New mbed boards can be plugged into existing build machines, or a whole new machine can be used.

Before adding a new board, be sure to upgrade the interface to the latest firmware. Instructions for each platform can be found on their platform page on [https://developer.mbed.org](https://developer.mbed.org).

Be sure to install the necessary serial drivers. See below for an incomplete list of some of these drivers (you can also always check the platform's page on [https://developer.mbed.org](https://developer.mbed.org) for instructions):
- DAPLink and CMSIS-DAP boards - [mbed serial driver](https://developer.mbed.org/handbook/Windows-serial-configuration)
- ST boards - [ST Link driver](https://developer.mbed.org/teams/ST/wiki/ST-Link-Driver)
- Nuvoton boards - [NuMaker USB Driver](http://www.nuvoton.com/hq/resource-download.jsp?tp_GUID=SW0520150729202535)

#### Creating a new test machine
I recommend using a Windows machine as that seems to be the most stable.

When you're creating a new physical group of test boards (on either a new machine or an existing build machine), you'll need to create a new Jenkins node. The instructions are the same as [setting up a build node](#jenkins-node-setup), however you'll need to change the labels.

For test nodes, you need to add the `test_node` label, as well `target_*`  labels that match the targets plugged into the machine. For instance, if the machine has a K64F and an ARCH_PRO connected and was named `test_node_8`, you would add the following labels:

```
test_node test_node_8 target_K64F target_ARCH_PRO
```

**NOTE:** Target names are uppercase and should match the name used by [mbed-ls](https://github.com/ARMmbed/mbed-ls).

#### Adding to an existing test machines
When the board has the latest firmware and all the necessary drivers are installed on the test machine, plug the board into the machine. Be sure that the board enumerates correctly and shows up with `mbedls` (the `mount_point` and `serial_port` should not be `unknown`).

Next, go to the node in the Jenkins UI and add the the appropriate `target_*` label. For example, if you're adding the NUCLEO_F411RE platform, you would add the label `target_NUCLEO_F411RE`.

### Jenkins test jobs
This section outlines a few of the test jobs that are currently in use

- `tm_wrap` - This job finds all online test nodes, clones mbed-os, and creates a virtualenv for them with all the necessary Python dependencies
- `test_matrix` - This job imports the correct build from a `build_matrix` job run and runs the tests on a platform. The list of platforms are generated dynamically from all the test nodes' `target_*` labels.
- `test_matrix_wrapper_cleanup` - This job cleans up a given environment created by `tm_wrap`.